﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace CMS_Server.Models
{
	public class ApplicationDbContext : DbContext
	{
		public DbSet<Student> Students { get; set; }
   
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
		{
		}
    }
}


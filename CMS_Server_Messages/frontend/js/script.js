const addButton = document.querySelector('.main-button-add');
const modal = document.getElementById('myModal');
const closeButton = document.querySelector('.close');
const addStudentForm = document.getElementById('addStudentForm');
const editButtons = document.querySelectorAll('.edit-button');
const removeButtons = document.querySelectorAll('.remove-button');
const removeStudentModal = document.getElementById('removeStudentModal');
const closeRemoveButton = document.querySelector('.close-remove');
const confirmRemoveButton = document.getElementById('confirmRemove');
const cancelRemoveButton = document.getElementById('cancelRemove');

const groupFeedback = document.querySelector('#Group-feedback');
const NameFeedback = document.querySelector('#Name-feedback');
const SurnameFeedback = document.querySelector('#Surname-feedback');
const genderFeedback = document.querySelector('#Gender-feedback');
const birthdayFeedback = document.querySelector('#Birthday-feedback');


let rowToRemove = null;

const UCD_SERVER_URL = "https://localhost:7179/";

window.addEventListener('load', function(event) 
{
    getAllStudentServerRequest
    (
        UCD_SERVER_URL + 'Students/GetAllStudents', 
        function(responseText)
        {
            let responseObject = JSON.parse(responseText);

            if(responseObject.Status === true)
            {
                let allStudentsArray = responseObject.Object;
                allStudentsArray.forEach(student => {
                    addNewStudentsTableRow(
                        student.Id, 
                        student.Group, 
                        student.FirstName + ' ' + student.LastName, 
                        student.Gender, 
                        formatBirthday(student.Birthday)
                    );
                });
                setupCheckboxEventListeners();
            }
        }
    );
});

function addNewStudentsTableRow(id, group, name, gender, birthday)
{
    const newRow = document.createElement('tr');
            newRow.innerHTML = `
                <td><input type="checkbox" class="table-checkbox"/></td>
                <td>${group}</td>
                <td>${name}</td>
                <td>${gender}</td>
                <td>${birthday}</td> 
                <td class="table-status" ><i class="fa-solid fa-circle"></i></td>
                <td>
                    <button class="edit-button"><i class="fa-solid fa-pen"></i></button>
                    <button class="remove-button"><i class="fa-solid fa-user-xmark"></i></button>
                </td>
            `;
            const tableBody = document.querySelector('.main-table tbody');
            tableBody.appendChild(newRow);

            newRow.querySelector('.edit-button').addEventListener('click', handleEdit);
            newRow.querySelector('.remove-button').addEventListener('click', handleRemove);
            newRow.setAttribute('data-id', id);
}

addButton.addEventListener('click', () => {
    document.getElementById('modal-content-title').textContent = 'Add Student';
    document.querySelector('#addStudentForm button[type="submit"]').textContent = 'Submit';
    
    modal.style.display = 'block';
});

closeButton.addEventListener('click', () => {

    resetModalAndForm();
    resetValidationStylesForAddEditForm()
});

editButtons.forEach(button => {
    button.addEventListener('click', handleEdit);
    
});

removeButtons.forEach(button => {
    button.addEventListener('click', handleRemove);
});


confirmRemoveButton.addEventListener('click', () => {
    if (rowToRemove !== null) {
        deleteStudentServerRequest
    (
        UCD_SERVER_URL + 'Students/DeleteStudent', 
        rowToRemove.getAttribute('data-id'),
        function(responseText)
        {
            let responseObject = JSON.parse(responseText);

            if(responseObject.Status === true)
            {
                rowToRemove.remove();
            }
        }
    ); 
        rowToRemove = null; 
        removeStudentModal.style.display = 'none'; 
    }
});

cancelRemoveButton.addEventListener('click', () => {
    rowToRemove = null; 
    removeStudentModal.style.display = 'none'; 
    resetValidationStylesForAddEditForm()
});




addStudentForm.addEventListener('submit', (event) => {
    event.preventDefault();
  
    const group = document.getElementById('group').value;
    const name = document.getElementById('name').value;
    const surname = document.getElementById('surname').value;
    const gender = document.getElementById('gender').value;
    const birthday = document.getElementById('birthday').value;
  
    const editingRow = document.querySelector('.editing');
    if (editingRow) {
      
  
      editStudentServerRequest(
        UCD_SERVER_URL + 'Students/EditStudent',
        editingRow.getAttribute('data-id'),
        group,
        name,
        surname,
        gender,
        birthday,
        (responseText) => {
          let responseObject = JSON.parse(responseText);
  
          if (responseObject.Status === false) {
            let fieldsArray = [
              document.getElementById('group'),
              document.getElementById('name'),
              document.getElementById('surname'),
              document.getElementById('gender'),
              document.getElementById('birthday')
            ];
            let validationFeedbacksArray = [
              groupFeedback,
              NameFeedback,
              SurnameFeedback,
              genderFeedback,
              birthdayFeedback
            ];
  
            let errorsArray = responseObject.ErrorObject.message.split("&&");
  
            for (let i = 0; i < 5; ++i) {
              if (errorsArray[i] === "") {
                setFieldValidStyles(fieldsArray[i], validationFeedbacksArray[i]);
              } else {
                setFieldInvalidStyles(fieldsArray[i], validationFeedbacksArray[i], errorsArray[i]);
              }
            }
          } else {
            editingRow.cells[1].textContent = group;
            editingRow.cells[2].textContent = `${name} ${surname}`;
            editingRow.cells[3].textContent = gender;
            editingRow.cells[4].textContent = formatBirthday(birthday);
            editingRow.classList.remove('editing');
  
            resetValidationStylesForAddEditForm();
            addStudentForm.reset();
            modal.style.display = 'none';
            setupCheckboxEventListeners();
          }
        }
      );
    } else {
      addNewStudentServerRequest(
        UCD_SERVER_URL + 'Students/AddStudent',
        group,
        name,
        surname,
        gender,
        birthday,
        (responseText) => {
          let responseObject = JSON.parse(responseText);
  
          if (responseObject.Status === false) {
            let fieldsArray = [
              document.getElementById('group'),
              document.getElementById('name'),
              document.getElementById('surname'),
              document.getElementById('gender'),
              document.getElementById('birthday')
            ];
            let validationFeedbacksArray = [
              groupFeedback,
              NameFeedback,
              SurnameFeedback,
              genderFeedback,
              birthdayFeedback
            ];
  
            let errorsArray = responseObject.ErrorObject.message.split("&&");
  
            for (let i = 0; i < 5; ++i) {
              if (errorsArray[i] === "") {
                setFieldValidStyles(fieldsArray[i], validationFeedbacksArray[i]);
              } else {
                setFieldInvalidStyles(fieldsArray[i], validationFeedbacksArray[i], errorsArray[i]);
              }
            }
          } else {
            addNewStudentsTableRow(responseObject.Object.Id, responseObject.Object.Group, responseObject.Object.FirstName + ' ' + responseObject.Object.LastName, responseObject.Object.Gender, formatBirthday(responseObject.Object.Birthday));
            resetValidationStylesForAddEditForm();
            addStudentForm.reset();
            modal.style.display = 'none';
            setupCheckboxEventListeners();
          }
        }
      );
    }
  });
  





function handleEdit(event) {
    const row = event.target.closest('tr');
    const cells = row.querySelectorAll('td');

    document.getElementById('group').value = cells[1].textContent;
    const nameSurname = cells[2].textContent.split(' ');
    document.getElementById('name').value = nameSurname[0];
    document.getElementById('surname').value = nameSurname[1];
    document.getElementById('gender').value = cells[3].textContent;
    document.getElementById('birthday').value = unformatBirthday(cells[4].textContent);
    
    
    document.getElementById('modal-content-title').textContent = 'Edit Student';
    document.querySelector('#addStudentForm button[type="submit"]').textContent = 'Save';

    row.classList.add('editing');
    modal.style.display = 'block';
}



function formatBirthday(birthday) {
    const [year, month, day] = birthday.split('-');
    return `${day}.${month}.${year}`;
}

function unformatBirthday(birthday) {
    const [day, month, year] = birthday.split('.');
    return `${year}-${month}-${day}`;
}

function setupCheckboxEventListeners() {
    const checkboxes = document.querySelectorAll('.table-checkbox');

    checkboxes.forEach(checkbox => {
        checkbox.addEventListener('change', function () {
            const statusCell = this.parentNode.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling;
            statusCell.style.color = this.checked ? 'green' : '#aaa';
        });
    });
}




function resetModalAndForm() {
    
    document.getElementById('group').value = ""; 
    document.getElementById('name').value = "";
    document.getElementById('surname').value = "";
    document.getElementById('gender').value = ""; 
    document.getElementById('birthday').value = "";


    document.getElementById('group').selectedIndex = 0;
    document.getElementById('gender').selectedIndex = 0;
    
   
    const editingRow = document.querySelector('.editing');
    if (editingRow) {
        editingRow.classList.remove('editing');
    }

    
    modal.style.display = 'none';
}
function handleRemove(event) {
    rowToRemove = event.target.closest('tr'); 
    removeStudentModal.style.display = 'block'; 
}



// ---------------------------------------------------------------------------------------------------------------------
// Server communication functions





/**
 * @param {string} requestUrl
 * @param {string} group - group field value
 * @param {string} firstName - firstName field value
 * @param {string} lastName - lastName field value
 * @param {string} gender - gender field value
 * @param {string} birthday - birthday field value
 * @param {function(string): void} actionAfterResponse - function,  which receives a response from the server as a parameter "responseText (string)" and performs actions on it
 * @returns {void}
*/
function addNewStudentServerRequest(requestUrl, group, firstName, lastName, gender, birthday, actionAfterResponse)
{
    const data = {
        Group: group,
        FirstName: firstName,
        LastName: lastName,
        Gender: gender,
        Birthday: birthday
    };
    console.log("POST request data: \n\n", data);

    const xhr = new XMLHttpRequest();
    xhr.open("POST", requestUrl, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                console.log('Success.\n\nResponse text:\n\n', xhr.responseText);
                actionAfterResponse(xhr.responseText);
            } else {
                console.error('Error:', xhr.responseText);
            }
        }
    };

    xhr.send(JSON.stringify(data));

    console.log("POST add student request sent to:", requestUrl);
}


/**
 * @param {string} requestUrl
 * @param {number} id - student id
 * @param {string} group - group field value
 * @param {string} firstName - firstName field value
 * @param {string} lastName - lastName field value
 * @param {string} gender - gender field value
 * @param {string} birthday - birthday field value
 * @param {function(string): void} actionAfterResponse - function,  which receives a response from the server as a parameter "responseText (string)" and performs actions on it
 * @returns {void}
*/
function editStudentServerRequest(requestUrl, id, group, firstName, lastName, gender, birthday, actionAfterResponse)
{
    const data = {
        Id: id,
        Group: group,
        FirstName: firstName,
        LastName: lastName,
        Gender: gender,
        Birthday: birthday
    };
    console.log("POST request data: \n\n", data);

    const xhr = new XMLHttpRequest();
    xhr.open("POST", requestUrl, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                console.log('Success.\n\nResponse text:\n\n', xhr.responseText);
                actionAfterResponse(xhr.responseText);
            } else {
                console.error('Error:', xhr.responseText);
            }
        }
    };

    xhr.send(JSON.stringify(data));

    console.log("POST edit student request sent to:", requestUrl);
}


/**
 * @param {string} requestUrl
 * @param {function(string): void} actionAfterResponse - function,  which receives a response from the server as a parameter "responseText (string)" and performs actions on it
 * @returns {void}
*/
function getAllStudentServerRequest(requestUrl, actionAfterResponse)
{
    const xhr = new XMLHttpRequest();
    xhr.open("GET", requestUrl, true);

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                console.log('Success.\n\nResponse text:\n\n', xhr.responseText);
                actionAfterResponse(xhr.responseText);
            } else {
                console.error('Error:', xhr.responseText);
            }
        }
    };

    xhr.send();

    console.log("GET all student request sent to:", requestUrl);
}


/**
 * @param {string} requestUrl
 * @param {number} studentId - student id
 * @param {function(string): void} actionAfterResponse - function,  which receives a response from the server as a parameter "responseText (string)" and performs actions on it
 * @returns {void}
*/
function deleteStudentServerRequest(requestUrl, studentId,  actionAfterResponse)
{
    const xhr = new XMLHttpRequest();
    const url = `${requestUrl}/${studentId}`; // Додаємо studentId до URL-адреси

    xhr.open("DELETE", url, true);

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                console.log('Success.\n\nResponse text:\n\n', xhr.responseText);
                actionAfterResponse(xhr.responseText);
            } else {
                console.error('Error:', xhr.responseText);
            }
        }
    };

    xhr.send();

    console.log("DELETE student request sent to:", url);
}

function setFieldInvalidStyles(field, fieldFeedbackContainer, errorText)
{
    field.classList.remove("valid-input");
    field.classList.add("invalid-input");

    fieldFeedbackContainer.innerText = errorText;
    fieldFeedbackContainer.classList.remove("valid-feedback");
    fieldFeedbackContainer.classList.add("invalid-feedback");
}


function setFieldValidStyles(field, fieldFeedbackContainer)
{
    field.classList.remove("invalid-input");
    field.classList.add("valid-input");

    fieldFeedbackContainer.innerText = "";
    fieldFeedbackContainer.classList.remove("invalid-feedback");
    fieldFeedbackContainer.classList.add("valid-feedback");
}


function resetValidationStylesForAddEditForm()
{
    let addEditFormFields = [document.getElementById('group'), document.getElementById('name'), document.getElementById('surname'), document.getElementById('gender'), document.getElementById('birthday')];

    for(let i = 0; i < addEditFormFields.length; ++i)
    {
        addEditFormFields[i].classList.remove("valid-input");
        addEditFormFields[i].classList.remove("invalid-input");
    }

    let feedbackContainers = [groupFeedback, NameFeedback, SurnameFeedback,
        genderFeedback, birthdayFeedback];

    for(let i = 0; i < feedbackContainers.length; ++i)
    {
        feedbackContainers[i].classList.remove("valid-feedback");
        feedbackContainers[i].classList.remove("invalid-feedback");
        feedbackContainers[i].innerText = "";
    }
}








